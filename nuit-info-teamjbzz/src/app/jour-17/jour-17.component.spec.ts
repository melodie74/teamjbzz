import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour17Component } from './jour-17.component';

describe('Jour17Component', () => {
  let component: Jour17Component;
  let fixture: ComponentFixture<Jour17Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour17Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
