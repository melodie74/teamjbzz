import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour8Component } from './jour-8.component';

describe('Jour8Component', () => {
  let component: Jour8Component;
  let fixture: ComponentFixture<Jour8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
