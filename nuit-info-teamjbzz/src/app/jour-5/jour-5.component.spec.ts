import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour5Component } from './jour-5.component';

describe('Jour5Component', () => {
  let component: Jour5Component;
  let fixture: ComponentFixture<Jour5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
