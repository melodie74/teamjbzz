import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour20Component } from './jour-20.component';

describe('Jour20Component', () => {
  let component: Jour20Component;
  let fixture: ComponentFixture<Jour20Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour20Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
