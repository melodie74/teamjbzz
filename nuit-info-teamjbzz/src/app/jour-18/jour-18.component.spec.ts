import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour18Component } from './jour-18.component';

describe('Jour18Component', () => {
  let component: Jour18Component;
  let fixture: ComponentFixture<Jour18Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour18Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour18Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
