import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour24Component } from './jour-24.component';

describe('Jour24Component', () => {
  let component: Jour24Component;
  let fixture: ComponentFixture<Jour24Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour24Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
