import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour16Component } from './jour-16.component';

describe('Jour16Component', () => {
  let component: Jour16Component;
  let fixture: ComponentFixture<Jour16Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour16Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour16Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
