import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour10Component } from './jour-10.component';

describe('Jour10Component', () => {
  let component: Jour10Component;
  let fixture: ComponentFixture<Jour10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
