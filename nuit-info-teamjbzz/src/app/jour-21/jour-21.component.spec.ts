import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour21Component } from './jour-21.component';

describe('Jour21Component', () => {
  let component: Jour21Component;
  let fixture: ComponentFixture<Jour21Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour21Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour21Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
