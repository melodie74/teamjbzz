import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour6Component } from './jour-6.component';

describe('Jour6Component', () => {
  let component: Jour6Component;
  let fixture: ComponentFixture<Jour6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
