import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour13Component } from './jour-13.component';

describe('Jour13Component', () => {
  let component: Jour13Component;
  let fixture: ComponentFixture<Jour13Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour13Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour13Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
