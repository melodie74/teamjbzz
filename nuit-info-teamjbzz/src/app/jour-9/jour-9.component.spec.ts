import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour9Component } from './jour-9.component';

describe('Jour9Component', () => {
  let component: Jour9Component;
  let fixture: ComponentFixture<Jour9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
