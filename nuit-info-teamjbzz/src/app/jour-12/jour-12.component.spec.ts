import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour12Component } from './jour-12.component';

describe('Jour12Component', () => {
  let component: Jour12Component;
  let fixture: ComponentFixture<Jour12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
