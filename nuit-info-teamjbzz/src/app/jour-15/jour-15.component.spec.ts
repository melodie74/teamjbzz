import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour15Component } from './jour-15.component';

describe('Jour15Component', () => {
  let component: Jour15Component;
  let fixture: ComponentFixture<Jour15Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour15Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour15Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
