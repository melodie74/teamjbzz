import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour11Component } from './jour-11.component';

describe('Jour11Component', () => {
  let component: Jour11Component;
  let fixture: ComponentFixture<Jour11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour11Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
