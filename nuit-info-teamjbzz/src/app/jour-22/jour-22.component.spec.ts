import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour22Component } from './jour-22.component';

describe('Jour22Component', () => {
  let component: Jour22Component;
  let fixture: ComponentFixture<Jour22Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour22Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour22Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
