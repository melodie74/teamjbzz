import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour14Component } from './jour-14.component';

describe('Jour14Component', () => {
  let component: Jour14Component;
  let fixture: ComponentFixture<Jour14Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour14Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour14Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
