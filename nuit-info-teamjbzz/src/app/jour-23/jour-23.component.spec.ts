import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour23Component } from './jour-23.component';

describe('Jour23Component', () => {
  let component: Jour23Component;
  let fixture: ComponentFixture<Jour23Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour23Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour23Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
