import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CalendrierGlobalComponent } from './calendrier-global/calendrier-global.component';
import { Jour1Component } from './jour-1/jour-1.component';
import { Jour2Component } from './jour-2/jour-2.component';
import { Jour3Component } from './jour-3/jour-3.component';
import { Jour4Component } from './jour-4/jour-4.component';
import { Jour5Component } from './jour-5/jour-5.component';
import { Jour6Component } from './jour-6/jour-6.component';
import { Jour7Component } from './jour-7/jour-7.component';
import { Jour8Component } from './jour-8/jour-8.component';
import { Jour9Component } from './jour-9/jour-9.component';
import { Jour10Component } from './jour-10/jour-10.component';
import { Jour11Component } from './jour-11/jour-11.component';
import { Jour12Component } from './jour-12/jour-12.component';
import { Jour13Component } from './jour-13/jour-13.component';
import { Jour14Component } from './jour-14/jour-14.component';
import { Jour15Component } from './jour-15/jour-15.component';
import { Jour16Component } from './jour-16/jour-16.component';
import { Jour17Component } from './jour-17/jour-17.component';
import { Jour18Component } from './jour-18/jour-18.component';
import { Jour19Component } from './jour-19/jour-19.component';
import { Jour20Component } from './jour-20/jour-20.component';
import { Jour21Component } from './jour-21/jour-21.component';
import { Jour22Component } from './jour-22/jour-22.component';
import { Jour23Component } from './jour-23/jour-23.component';
import { Jour24Component } from './jour-24/jour-24.component';


@NgModule({
  declarations: [
    AppComponent,
    CalendrierGlobalComponent,
    Jour1Component,
    Jour2Component,
    Jour3Component,
    Jour4Component,
    Jour5Component,
    Jour6Component,
    Jour7Component,
    Jour8Component,
    Jour9Component,
    Jour10Component,
    Jour11Component,
    Jour12Component,
    Jour13Component,
    Jour14Component,
    Jour15Component,
    Jour16Component,
    Jour17Component,
    Jour18Component,
    Jour19Component,
    Jour20Component,
    Jour21Component,
    Jour22Component,
    Jour23Component,
    Jour24Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
