import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendrier-global',
  templateUrl: './calendrier-global.component.html',
  styleUrls: ['./calendrier-global.component.css']
})
export class CalendrierGlobalComponent implements OnInit {

  pagePrincipal = true;
  pageUn = false;
  pageDeux = false;
  pageTrois = false;
  pageQuatre = false;
  pageCinq = false;
  pageSix = false;
  pageSept = false;
  pageHuit = false;
  pageNeuf = false;
  pageDix = false;
  pageOnze = false;
  pageDouze = false;
  pageTreize = false;
  pageQuatorze = false;
  pageQuinze = false;
  pageSeize = false;
  pageDixSept = false;
  pageDixHuit = false;
  pageDixNeuf = false;
  pageVingt = false;
  pageVingtUn = false;
  pageVingtDeux = false;
  pageVingtTrois = false;
  pageVingtQuatre = false;

  constructor() { }

  ngOnInit() {
  }

  case1(page: boolean ) {
    this.pageUn = true;
    this.pagePrincipal = false;
  }
  case2(page: boolean ) {
    this.pageDeux = true;
    this.pagePrincipal = false;
  }
  case3(page: boolean ) {
    this.pageTrois = true;
    this.pagePrincipal = false;
  }
  case4(page: boolean ) {
    this.pageQuatre = true;
    this.pagePrincipal = false;
  }
  case5(page: boolean ) {
    this.pageCinq = true;
    this.pagePrincipal = false;
  }
  case6(page: boolean ) {
    this.pageSix = true;
    this.pagePrincipal = false;
  }
  case7(page: boolean ) {
    this.pageSept = true;
    this.pagePrincipal = false;
  }
  case8(page: boolean ) {
    this.pageHuit = true;
    this.pagePrincipal = false;
  }
  case9(page: boolean ) {
    this.pageNeuf = true;
    this.pagePrincipal = false;
  }
  case10(page: boolean ) {
    this.pageDix = true;
    this.pagePrincipal = false;
  }
  case11(page: boolean ) {
    this.pageOnze = true;
    this.pagePrincipal = false;
  }
  case12(page: boolean ) {
    this.pageDouze = true;
    this.pagePrincipal = false;
  }
  case13(page: boolean ) {
    this.pageTreize = true;
    this.pagePrincipal = false;
  }
  case14(page: boolean ) {
    this.pageQuatorze = true;
    this.pagePrincipal = false;
  }
  case15(page: boolean ) {
    this.pageQuinze = true;
    this.pagePrincipal = false;
  }
  case16(page: boolean ) {
    this.pageSeize = true;
    this.pagePrincipal = false;
  }
  case17(page: boolean ) {
    this.pageDixSept = true;
    this.pagePrincipal = false;
  }
  case18(page: boolean ) {
    this.pageDixHuit = true;
    this.pagePrincipal = false;
  }
  case19(page: boolean ) {
    this.pageDixNeuf = true;
    this.pagePrincipal = false;
  }
  case20(page: boolean ) {
    this.pageVingt = true;
    this.pagePrincipal = false;
  }
  case21(page: boolean ) {
    this.pageVingtUn = true;
    this.pagePrincipal = false;
  }
  case22(page: boolean ) {
    this.pageVingtDeux = true;
    this.pagePrincipal = false;
  }
  case23(page: boolean ) {
    this.pageVingtTrois = true;
    this.pagePrincipal = false;
  }
  case24(page: boolean ) {
    this.pageVingtQuatre = true;
    this.pagePrincipal = false;
  }


}
