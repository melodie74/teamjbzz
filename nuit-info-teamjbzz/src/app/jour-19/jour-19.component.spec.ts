import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour19Component } from './jour-19.component';

describe('Jour19Component', () => {
  let component: Jour19Component;
  let fixture: ComponentFixture<Jour19Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour19Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour19Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
