import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour7Component } from './jour-7.component';

describe('Jour7Component', () => {
  let component: Jour7Component;
  let fixture: ComponentFixture<Jour7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
