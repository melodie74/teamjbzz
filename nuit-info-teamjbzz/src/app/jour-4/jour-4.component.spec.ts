import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Jour4Component } from './jour-4.component';

describe('Jour4Component', () => {
  let component: Jour4Component;
  let fixture: ComponentFixture<Jour4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Jour4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Jour4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
